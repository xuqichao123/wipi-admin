import { RequestConfig } from 'umi';

export const layout = () => {
  return {
      rightContentRender: () => <div>rightContentRender</div>,
      footerRender: () => <div>footer</div>,
      onPageChange: () => {
        console.log('onPageChange')
      },
      menuHeaderRender: undefined,
  }
}

export const request: RequestConfig = {
  timeout: 1000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [],
  responseInterceptors: [],
};

export interface InitialState {
  role:string,
  [propName:string]:any
}

export const getInitialState = async () => {
  try {
    const userInfo = JSON.parse(localStorage.getItem('userInfo') as string);
    return userInfo || {
      role:''
    };
  }catch(error){
    return {
      role:''
    }
  }
}