import { request } from 'umi';

export interface loginData {
  name:string,
  password:string
}

export const login = (data:loginData) => request('/login', {
  method: "post",
  data,
  skipErrorHandler: true,
})