export default [
  {
    path:'/login',
    exact: true,
    component:"@/pages/login",
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,// 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
  },
  {
    path:'/registry',
    exact: true,
    component:"@/pages/registry",
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,// 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
  },
  {
    path:'/',
    exact: true,
    name: "工作台",
    component:"@/pages/index"
  },
  {
    name:"文章管理",
    path:'/article',
    routes:[
      {
        path:"/article/all",
        name:"所有文章",
        component:"@/pages/article/all"
      },
      {
        path:"/article/type",
        name:"文章分类",
        component:"@/pages/article/type"
      },
      {
        path:"/article/tags",
        name:"文章标签",
        component:"@/pages/article/tags"
      },
    ]
  },
  {
    path:'/user',
    exact: true,
    name:"用户管理",
    access: "canUser",
    component:"@/pages/user"
  },
]